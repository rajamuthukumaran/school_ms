const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const t1 = require('./routes/t1');
const t2 = require('./routes/t2');
const t3 = require('./routes/t3');
const auth = require('./routes/auth')

require('dotenv/config');

const app = express();

app.use(express.json());
app.use(cors())

app.use('/api', t1);
app.use('/api', t2);
app.use('/api', t3);
app.use('/auth', auth)

const uri = process.env.DB_URI;
const port = process.env.REACT_APP_PORT || 3001;

mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("connected to DB");
})

app.listen(port, () => {
    console.log(`Server is running at ${port}`)
})