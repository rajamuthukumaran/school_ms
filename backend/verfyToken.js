const jwt = require('jsonwebtoken');

const tokenValidation = (req, res, next) => {
    const token = req.header('authtoken');
    if(!token) return res.status(401).send('Access Denied')

    try{
        const verified = jwt.verify(token, process.env.TOKEN_KEY);
        req.user = verified;
        next();
    }
    catch(err){
        return res.status(400).send({error: 'Invalid token', status: false})
    }
}

module.exports = tokenValidation