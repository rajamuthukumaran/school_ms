const router = require('express').Router();
const User = require('../model/userModel');
const Student = require('../model/studentModel');
const verify = require('../verfyToken');

router.patch('/password/:uname', verify, async (req, res) => {
    const { uname } = req.params;
    const { password } = req.body;
    let check = await User.findOne({ username: uname });

    if(check){
        User.updateOne({username: uname}, {$set: {
            password
        }})
            .then(data => res.status(201).json(data))
            .catch(err => res.status(400).send({error: err, status: false}) )
    }
    else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.get('/student/:username', verify, (req, res) => {
    const { username } = req.params;
    Student.findOne({username})
            .then(user => res.json(user))
            .catch(err => res.status(400).send({error: err, status: false}) )
})

router.get('/emp/:uname', verify, (req, res) => {
    const { uname } = req.params;
    User.findOne({username: uname})
        .then(user => {
            const { username, name, dob, blood, father, designation, rm, address } = user;
            return res.json({ username, name, dob, blood, father, designation, rm, address })
        })
        .catch(err => res.status(400).send({error: err, status: false}) )
})

router.get('/check/:username', verify, (req, res) => {
    const { username } = req.params;
    User.findOne({username})
        .then(user => res.send({user}))
        .catch(() => res.send({user}) )
})

module.exports = router;