const router = require('express').Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const User = require('../model/userModel')
const verify = require('../verfyToken');
// const Session = require('../model/sessionModel')

router.post('/login', async (req, res) => {
    const { username, password } = req.body;

    const check = await User.findOne({username});
    if(!check) return res.status(400).send({Error: 'User not found', status: false});

    const validate = await bcrypt.compare(password, check.password);
    if(!validate) return res.status(400).send({Error: "Invalid password", status: false});
    
    const token = jwt.sign({username}, process.env.TOKEN_KEY, {expiresIn: 60 * 60})
    res.header('authtoken', token).send(token)

    // const checkSession = await Session.findOne({username})
    // if(checkSession) return res.status(401).send({error: "Already Logged in please log out to continue", status: false})
    // const createSession = new Session({username})
    // const session = createSession.save()
    // if(!session) return res.status(500).send({error: "Unable to create session", status: false})
})

router.post('/check', verify, async (req, res) => {
    const { username, password } = req.body;
    const check = await User.findOne({username});
    if(!check) return res.send({Error: 'User not found', status: false});

    const validate = await bcrypt.compare(password, check.password);
    if(!validate) return res.send({Error: "Invalid password", status: false});

    return res.status(200).send({status: true})
})

module.exports = router