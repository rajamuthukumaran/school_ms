const router = require('express').Router();
const User = require('../model/userModel');
const Student = require('../model/studentModel');
const verify = require('../verfyToken');

router.get('/', verify, (req, res) => {
    User.find()
        .then(users => {
            const rtnData = users.map( i => {
                const { username, name, dob, blood, father, designation, rm, address } = i;
                return { username, name, dob, blood, father, designation, rm, address };
            })
            return res.json(rtnData)
        })
        // .then(users => res.json(users))
        .catch(err => res.status(400).send({error: err, status: false}) )
})

router.get('/student', verify, (req, res) => {
    Student.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).send({error: err, status: false}) )
})

router.patch('/student/attendence/:uname', verify, async (req, res) => {
    const username = req.params.uname;
    let check = await User.findOne({ username: username });
    if (check) {
        let data = await Student.findOne({ username: username })

        if (data) {
            const prev_attend = data.attendence;
            prev_attend.push({
                date: req.body.date,
                present: req.body.present
            });

            Student.update({ username: username }, { $set: { attendence: prev_attend } })
                .then(data => res.status(201).json(data))
                .catch(err => res.status(400).send({error: err, status: false}) )
        }
    } else {
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/student/exam/:uname', verify, async (req, res) => {
    const username = req.params.uname;
    let check = await Student.findOne({ username: username });

    if (check) {
        const { math, english, language, science, history } = check.marks;
        const sMath = req.body.math ? req.body.math : math? math: 'na';
        const sEnglish = req.body.english ? req.body.english : english? english: 'na';
        const sLanguage = req.body.language ? req.body.language : language? language: 'na';
        const sScience = req.body.science ? req.body.science : science? science: 'na';
        const sHistory = req.body.history ? req.body.history : history? history: 'na';
        const marks = {
            math: sMath,
            english: sEnglish,
            language: sLanguage,
            science: sScience,
            history: sHistory
        }

        Student.update({ username: username }, { $set: {marks: marks} })
                .then(data => res.status(201).json(data))
                .catch(err => res.status(400).send({error: err, status: false}) )
    } else {
        return res.status(400).send({error: 'User not found', status: false});
    }
})

module.exports = router;