const router = require('express').Router();
const bcrypt = require('bcryptjs')
const User = require('../model/userModel');
const Student = require('../model/studentModel');
const Jobs = require('../model/empModel');
const verify = require('../verfyToken');

router.post('/', verify, async (req, res) => {
    const { username, password, name, dob, father, blood, address, designation, rm } = req.body;

    const salt = await bcrypt.genSalt(10);
    const encrypted = await bcrypt.hash(password, salt)

    const post = new User({
        username, password: encrypted, name, dob, father, blood, address, designation, rm
    })

    if(req.body.designation === 3){
        const postStudent = new Student({
            username: req.body.username
        })

        postStudent.save()
            .then( data => res.status(201).json(data))
            .catch( err => res.status(400).send({error: err, status: false}) )
    }

    post.save()
        .then( data => res.status(201).send(`Successfully added ${username}`))
        .catch( err => res.status(400).send({error: err, status: false}) )
})

router.put('/:uname', verify, async (req, res) => {
    const { uname } = req.params;
    const { username, password, name, dob, father, blood, address, designation, rm } = req.body;

    const salt = await bcrypt.genSalt(10);
    const encrypted = await bcrypt.hash(password, salt)

    const check = await User.findOne({username: uname})

    if(!check) return res.status(404).send({status: false, error: "username not found"})

    let update;

    try{
        update = await User.updateOne({username: uname}, { $set: {
            username, password: encrypted, name, dob, father, blood, address, designation, rm
        }})
    }
    catch(err){
        return res.status(400).send({error: err, status: false})
    }
    return res.status(201).send({status: true, data: update})
})

router.delete('/:username', verify, async (req, res) => {
    const username = req.params.username;
    const userdata = await User.find({username});
    if(userdata){
        const isStudent = (userdata.designation === 3)? true: false;
    
        User.remove({username})
            .then( data => res.status(201).json(data))
            .catch( err => res.status(400).send({error: err, status: false}) )

        if(isStudent){
            Student.remove({username})
                .then( data => res.status(201).json(data))
                .catch( err => res.status(400).send({error: err, status: false}) )
        }
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.post('/emp', verify, (req, res) => {
    const { id, designation } = req.body;
    const post = new Jobs({
        id,
        designation
    })

    post.save()
    .then( data => res.status(201).json(data))
    .catch( err => res.status(400).send({error: err, status: false}) )
})

router.patch('/rm/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {rm: req.body.rm}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/name/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {name: req.body.name}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/dob/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {dob: req.body.dob}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/father/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {father: req.body.father}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/blood/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {blood: req.body.blood}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/address/:username', verify, async (req, res) => {
    const { username } = req.params;
    const check = await User.find({username});
    if(check){
        User.updateOne({username}, {
            $set: {address: req.body.address}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).send({error: 'User not found', status: false});
    }
})

router.patch('/tier/:id', verify, async (req, res) => {
    const { id } = req.params;
    const check = await Jobs.find({id});
    if(check){
        Jobs.updateOne({id}, {
            $set: {tier: req.body.tier}
        })
        .then( data => res.status(201).json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
    }else{
        return res.status(400).json('error: Job not found');
    }
})

router.get('/job/:id', verify, async (req, res) => {
    const { id } = req.params;
    Jobs.findOne({id})
        .then(data => res.json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
})

router.get('/job', verify, (req, res) => {
    Jobs.find()
        .then(data => res.json(data))
        .catch( err => res.status(400).send({error: err, status: false}) )
})

router.delete('/admin/all', verify, async (req, res) => {
    User.collection.drop()
            .then( data => res.status(201).json(data))
            .catch( err => res.status(400).send({error: err, status: false}) );
    Student.collection.drop()
                .then( data => res.status(201).json(data))
                .catch( err => res.status(400).send({error: err, status: false}) );
})

module.exports = router;