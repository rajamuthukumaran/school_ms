const mongooose = require('mongoose');

const sessionModel = mongooose.Schema({
    username: {
        type: String,
        unique: true,
        require: true
    }
})

module.exports = mongooose.model('Session', sessionModel);