const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    username: {
        type: String,
        require: true,
        unique: true,
        lowercase: true,
        maxlength: 20
    },
    password: {
        type: String,
        minlength: 8,
        default: ''
    },
    name: {
        type: String,
        require: true,
        maxlength: 20
    },
    dob: {
        type: Date,
        require: true
    },
    father: {
        type: String,
        maxlength: 20
    },
    blood: {
        type: String,
        maxlength: 5
    },
    address: {
        type: String,
        maxlength: 100
    },
    designation: {
        type: Number,
        require: true,
        default: 3
    },
    rm: {
        type: String,
        require: true,
        default: 'na'
    }
})

module.exports = mongoose.model('User', userSchema);