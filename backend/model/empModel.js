const mongoose = require('mongoose');

const empModel = mongoose.Schema({
    id:{
        type: Number,
        require: true
    },
    designation: {
        type: String,
        require: true
    },
    tier: {
        type: Number,
        require: true,
        default: 3
    }
})

module.exports = mongoose.model('Jobs', empModel)