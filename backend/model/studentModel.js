const mongoose = require('mongoose');

const studentModel = mongoose.Schema({
    username: {
        type: String,
        require: true,
        unique: true,
        lowercase: true,
        maxlength: 20
    },
    attendence: [
        {
            date: {
                type: Date,
                unique: true
            },
            present: {
                type: Boolean,
                default: false
            }

        }
    ],
    marks: {
            math: String,
            english: String,
            language: String,
            science: String,
            history: String
        }
})

module.exports = mongoose.model('Student', studentModel)