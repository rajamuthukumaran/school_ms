import Axios from 'axios';

const URI = process.env.REACT_APP_URI;
const PORT = process.env.REACT_APP_PORT;
const API = `${URI}:${PORT}/api`;

export const addUser = async (userData, config) => {
    try{
        Axios.post(API, userData, config)
    }
    catch(err){
        return err;
    }
    return null
}

export const checkUser = async (id, config) => {
    const URL = `${URI}/check/${id}`;
    const check = Axios.get(URL, config)
    console.log(check.user)
    if(check.user){
        return true;
    }
    return false
}

export const getJobs = async (config) => {
    const URL = `${API}/job`
    let jobs;
    try{
        jobs = await Axios.get(URL, config)
    }
    catch(error){
        return {error, status: false};
    }
    return {jobs, status: true};
}

export const login = async (username, password) => {
    const URL = `${URI}:${PORT}/auth/login`;
    let token
    try{
        token = await Axios.post(URL, {username, password});
    }
    catch(error){
        return {error, status: false};
    }
    return {token, status: true}
}

export const getUserData = async (uname, config) => {
    const URL = `${API}/emp/${uname}`;
    let userData
    try{
        userData = await Axios.get(URL, config);
    }
    catch(error){
        return {error, status: false};
    }
    return { userData, status: true }
}


export const getJobData = async (designation, config) => {
    const URL = `${API}/job/${designation}`;
    let jobData 
    try{
        jobData = await Axios.get(URL, config);
    }
    catch(error){
        return {error, status: false};
    }
    return { jobData, status: true }
}
// export const getJobs = (config) => {
//     const URL = `${API}/job`
//     console.log(axios.get(URL, config).then(d => d))
// }