import React, { useState } from 'react';
import '../sass/maintenance.scss';
import AddUser from '../components/panels/AddUser';
import RmUser from '../components/panels/RmUser';
import UpdateUser from '../components/panels/UpdateUser';

const Maintenance = (props) => {
    const { userData, jobData, config } = props
    const [ panel, setPanel ] = useState('')
    const [ toast, setToast ] = useState('')
    const [ hide, setHide ] = useState(' hide')

    const handleToast = (msg) => {
        setHide('');
        setToast(msg);
        setTimeout(() => {
            setHide(' hide');
            setToast('');
        }, 5000)
    }

    return ( 
        <>
            <main className="maintenance">
                {panel}
                <div className="panel_btn_row1">
                    <div className="panel_btn add_user" onClick={() => setPanel(<AddUser setPanel={setPanel} setToast={handleToast} jobData={jobData} config={config}/>)}>
                        <p>Add User</p>
                    </div>
                    <div className="panel_btn remove_user" onClick={() => setPanel(<RmUser setPanel={setPanel} setToast={handleToast} jobData={jobData} config={config}/>)}>
                        <p>Remove User</p>
                    </div>
                    <div className="panel_btn update_user" onClick={() => setPanel(<UpdateUser setPanel={setPanel} setToast={handleToast} jobData={jobData} config={config}/>)}>
                        <p>Update User</p>
                    </div>
                    <div className="panel_btn assign_teacher">
                        <p>Assign Teachers</p>
                    </div>
                </div>
                <div className="panel_btn_row2">
                    <div className="panel_btn add_job">
                        <p>Add Job</p>
                    </div>
                    <div className="panel_btn modify_job">
                        <p>Modify Job</p>
                    </div>
                    <div className="panel_btn delete_job">
                        <p>Delete Job</p>
                    </div>
                </div>
            </main>
            <div className={"toast"+hide}><p>{toast}</p></div>
        </>
     );
}
 
export default Maintenance;