import React, { useState } from 'react';
import '../sass/home.scss';
import { Link } from 'react-router-dom';

const Home = (props) => {
    const { username, name, dob, father, blood, address, job, designation, sup } = props.userData;
    return ( 
        <main className='home'>
            <div className="pic"></div>
            <div className="details">
                <p><span>Name:</span>{name}</p>
                <p><span>Username:</span>{username}</p>
                <p><span>D.O.B:</span>{dob}</p>
                <p><span>Father:</span>{father}</p>
                <p><span>Blood:</span>{blood}</p>
                <p><span>Designation:</span>{job}</p>
                <p><span>Supervisor:</span>{sup}</p>
                <p><span>Address:</span>{address}</p>
                {(designation === 3) && <Link to="/marks"><button>Check Marks</button></Link>}
            </div>
        </main>
     );
}
 
export default Home;