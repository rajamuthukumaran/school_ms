import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../sass/staffs.scss';
import Cookie from 'js-cookie';

const Staffs = (props) => {
    const { jobData, config } = props;
    const [users, loadUsers] = useState([])
    const [radio, setRadio] = useState(0)
    const [ panel, setPanel ] = useState('')
    const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}/api`;

    useEffect(() => {
        Axios.get(API, config)
            .then(res => loadUsers(res.data))
        if (!users) {
            Cookie.remove('userData');
            Cookie.remove('jobData');
            Cookie.remove('Config');
            Cookie.remove('status');
            window.location.reload();
        }
    }, [])

    const jobOption = (data) => {
        const { id, designation } = data;
        return (
            <li key={id}><input type="radio" name="job" value={id} onChange={() => setRadio(id)} />{designation}</li>
        )
    }

    const displayUsers = () => {
        let usersData = users
        if (radio) {
            usersData = users.filter(i => i.designation === radio)
        }
        return usersData.map(i =>
            <li key={i.username}>{i.name}<button onClick={() => handleDetails(i.username)}>i</button></li>
        )
    }

    const handleDetails = (username) => {
        Axios.get(API+`/emp/${username}`, config)
            .then(res => setPanel(res.data.username))
    }

    return (
        <main className="staffs">
            {panel}
            <div className="option">
                <ul>
                    <li><input type="radio" name="job" value={0} defaultChecked onChange={() => setRadio(0)} />All</li>
                    {jobData.map(i => jobOption(i))}
                </ul>
            </div>
            <div className="list">
                <ul>
                    {displayUsers()}
                </ul>
            </div>
        </main>
    );
}

export default Staffs;