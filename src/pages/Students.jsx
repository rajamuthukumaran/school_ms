import React, { useState, useEffect } from 'react'
import Axios from 'axios';

const Students = ({ config }) => {
    const [ students, loadStudents ] = useState([])
    const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}/api/students`;

    useEffect(() => {
        Axios.get(API, config)
            .then(data => loadStudents(data))
    }, [])

    return ( 
        <main className="students">
            {students.map( i => console.log(i))}
        </main>
     );
}
 
export default Students;