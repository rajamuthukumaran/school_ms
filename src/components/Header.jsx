import React from 'react';
import '../sass/header.scss';
import { Link } from 'react-router-dom';
import Cookie from 'js-cookie'

const Header = (props) => {
    const { tier } = props;

    const handleLogout = () => {
        Cookie.remove('userData')
        Cookie.remove('config')
        Cookie.remove('jobData')
        Cookie.remove('status')
        window.location.reload()
    }

    return ( 
        <header>
            <nav>
                <ul>
                    { 
                        ( tier === 1 ) && 
                        <Link to="/maintenance">
                            <li>Maintenance</li>
                        </Link>
                    }
                    {
                        ( tier === 1 ) &&
                        <Link to= "/staffs">
                            <li>Staffs</li>
                        </Link>
                    }
                    {
                        (tier !== 3) && 
                        <Link to="/students">
                            <li>Students</li>
                        </Link>
                    }
                    <Link to="/">
                        <li>Profile</li>
                    </Link>
                    <li onClick={handleLogout}>Logout</li>
                </ul>
            </nav>
        </header>
     );
}
 
export default Header;