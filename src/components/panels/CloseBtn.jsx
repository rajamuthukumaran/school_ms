import React from 'react'

const CloseBtn = ({setPanel}) => {
    return(
        <div className="panel_close_btn" onClick={() => setPanel('')}>x</div>
    )
}

export default CloseBtn;