import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Cookie from 'js-cookie';
import Panel from './Panel';

const RmUser = ({ setPanel, setToast, config }) => {
    const [search, setSearch] = useState('')
    const [users, loadUsers] = useState([])
    const [dispUsers, setDispUsers] = useState([])
    const [rmUsers, setRmUsers] = useState([])
    const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}/api`;

    useEffect(() => {
        Axios.get(API, config)
            .then(res => {
                loadUsers(res.data)
                setDispUsers(res.data)
            })
        if (!users) logout();
    }, [])

    const handleList = e => {
        const uname = e.target.value;
        if (e.target.checked) {
            let adduser = [...rmUsers, uname];
            setRmUsers(adduser)
        }
        else {
            let rmUser = [...rmUsers];
            let index = rmUser.indexOf(uname)
            rmUser.splice(index, 1);
            setRmUsers(rmUser)
        }
        console.log(rmUsers)
    }

    const logout = () => {
        Cookie.remove('userData');
        Cookie.remove('jobData');
        Cookie.remove('Config');
        Cookie.remove('status');
        window.location.reload();
    }

    const searchFilter = (i, len) => {
        const str = i.name.slice(0, len)
        return search === str;
    }

    const handleSearchBar = e => {
        const { value } = e.target;
        setSearch(value);
        const len = value.length;
        let newDisp = users.filter(i => {
            return i.name.slice(0, len).toLowerCase() === value.toLowerCase() ||
                i.username.slice(0, len).toLowerCase() === value.toLowerCase()
        });
        setDispUsers(newDisp);
    }

    const displayUsers = (name, username) => {
        return (
            <li key={username}><input type="checkbox" value={username} onClick={handleList} />{`${name}`}<span>{`(${username})`}</span></li>
        )
    }

    const handleDelete = e => {
        e.preventDefault()
        rmUsers.map(i => deleteUser(i));
    }

    const deleteUser = (name) => {
        const URL = API + `/${name}`
        Axios.delete(URL, config)
            .then(() => {
                setPanel('')
                setToast(`${name} is successfully removed`)
            }
            )
            .catch(() => {
                setPanel('');
                setToast('Please relogin to continue...')
                setTimeout(() => window.location.reload(), 5000)
            })
    }

    return (
        <Panel name="rm_user" setPanel={setPanel}>
            <div className="search">
                <form>
                    <input type="text" placeholder="Enter the username" value={search} onChange={handleSearchBar} />
                    <button>Search</button>
                </form>
            </div>
            <div className="list">
                <form>
                    <ul>
                        {dispUsers.map(i => displayUsers(i.name, i.username))}
                    </ul>
                    <button onClick={handleDelete}>Delete</button>
                </form>
            </div>
        </Panel>
    );
}

export default RmUser;