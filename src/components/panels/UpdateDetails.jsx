import React, { useState, useEffect } from 'react'
import Panel from './Panel';
import Axios from 'axios';
import Cookie from 'js-cookie'

const Details = ({ setPanel, setToast, jobData, config, updUser }) => {
    const [name, setName] = useState(updUser.name)
    const [username, setUsername] = useState(updUser.username)
    const [password, setPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confPassword, setConfPassword] = useState('')
    const [dob, setDob] = useState(updUser.dob)
    const [father, setFather] = useState(updUser.father)
    const [blood, setBlood] = useState(updUser.blood)
    const [address, setAddress] = useState(updUser.address)
    const [rm, setRm] = useState(updUser.rm)
    const [designation, setDesignation] = useState(updUser.designation)
    const [status, setStatus] = useState(updUser.status)
    const [ updPassword, setUpdPassword ] = useState(false)
    const oldUsername = updUser.username
    const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}`;

    useEffect(() => {
        document.getElementById('upd_usr_desg').selectedIndex = designation - 1;
    }, [])

    const handleSubmit = async e => {
        e.preventDefault();
        const checkUsername = await Axios.get(`${API}/api/check/${username}`, config)
        const checkRM = await Axios.get(`${API}/api/check/${rm}`, config)
        const checkPassword = await Axios.post(API + `/auth/check`, { username, password }, config)
        if (username || name || dob || father || blood || address || rm) {
            if (username && (oldUsername === username || !checkUsername.data.user)) {
                // if (checkPassword.status) {
                    // if (!updPassword || password === confPassword) {
                        if (rm && checkRM.data.user) {
                            let psswd = updPassword? password : newPassword;
                            const data = {
                                username, password: psswd, name, dob, father, blood, address, designation, rm
                            }
                            Axios.put(API+`/api/${username}`, data, config)
                                .then(() => {
                                    setPanel('')
                                    setToast(`${username} is successfully added`)
                                })
                                .catch(() => {
                                    setPanel('');
                                    setToast('Please relogin to continue...')
                                })
                        } else {
                            setStatus("No such reporting manager found")
                        }
                    // }
                    // else {
                    //     setStatus("Password doesn't match")
                    // }
                // }
                // else {
                //     setStatus('Invalid password')
                // }
            }
            else {
                setStatus("Username already taken")
            }
        }
        else {
            setStatus("Please enter all the fields")
        }
    }

    return (
        <Panel name="update_det" setPanel={setPanel}>
            <form onSubmit={handleSubmit}>
                <p className="pstatus">{status}</p>
                <p><input type="text" placeholder="Enter the name" value={name} onChange={e => setName(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter a username" value={username} onChange={e => setUsername(e.target.value)} /></p>
                {/* <p><input type="password" placeholder="Enter a password" value={password} onChange={e => setPassword(e.target.value)} /></p> */}
                {/* <p><span>Do you wish to update password?</span><input class="updpsswd" name="updpsswd" type="radio" value={true} onClick={() => setUpdPassword(true)}/>Yes<input class="updpsswd" name="updpsswd" type="radio" value={false} defaultChecked onClick={() => setUpdPassword(false)}/>No</p> */}
                {/* {updPassword && <p><input type="password" placeholder="Enter a new password" value={newPassword} onChange={e => setNewPassword(e.target.value)} /></p>} */}
                {/* {updPassword && <p><input type="password" placeholder="confirm the password" value={confPassword} onChange={e => setConfPassword(e.target.value)} /></p>} */}
                <p><input type="text" placeholder="Enter the date of birth" value={dob} onChange={e => setDob(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter fathers name" value={father} onChange={e => setFather(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter blood group" value={blood} onChange={e => setBlood(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter the address" value={address} onChange={e => setAddress(e.target.value)} /></p>
                <p>
                    <span>Select the designation:</span>
                    <select id="upd_usr_desg" onChange={e => setDesignation(e.target.value)}>
                        {jobData.map(i => <option key={i.id} value={i.id}>{i.designation}</option>)}
                    </select>
                </p>
                <p><input type="text" placeholder="Enter the reporting manager" value={rm} onChange={e => setRm(e.target.value)} /></p>
                <button onClick={handleSubmit}>Update user</button>
            </form>
        </Panel>
    );
}

export default Details;