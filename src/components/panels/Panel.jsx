import React from 'react';
import CloseBtn from './CloseBtn';

const Panel = ({ name, setPanel, children }) => {
    return ( 
        <div className={`panel panel_${name}`}>
            <CloseBtn setPanel={setPanel}/>
            {children}
        </div>
     );
}
 
export default Panel;