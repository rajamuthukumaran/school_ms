import React, { useState } from 'react';
import Panel from './Panel';
import Axios from 'axios';

const AddUser = ({ setPanel, config, jobData, setToast }) => {
    const [name, setName] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confPassword, setConfPassword] = useState('')
    const [dob, setDob] = useState('')
    const [father, setFather] = useState('')
    const [blood, setBlood] = useState('')
    const [address, setAddress] = useState('')
    const [rm, setRm] = useState('')
    const [designation, setDesignation] = useState(jobData[0].id)
    const [status, setStatus] = useState('')

    const handleSubmit = async e => {
        e.preventDefault()
        setStatus('')
        const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}/api`;
        const checkUsername = await Axios.get(`${API}/check/${username}`, config)
        const checkRM = await Axios.get(`${API}/check/${rm}`, config)
        if (!username || !name || !dob || !father || !blood || !address || !rm) {
            setStatus("Please enter all the fields")
            return
        }
        else if (password !== confPassword) {
            setStatus("Password doesn't match")
            return
        }
        else if (username && checkUsername.data.user) {
            setStatus("Username already taken")
            return
        }
        else if (rm && !checkRM.data.user) {
            setStatus("No such reporting manager found")
            return
        }
        else {
            const data = {
                username, password, name, dob, father, blood, address, designation, rm
            }
            Axios.post(API, data, config)
                .then(() => {
                    setPanel('')
                    setToast(`${username} is successfully added`)
                })
                .catch(() => {
                    setPanel('');
                    setToast('Please relogin to continue...')
                    setTimeout(() => window.location.reload(), 5000)
                })
        }
    }

    return (
        <Panel name={'add_user'} setPanel={setPanel}>
            <form onSubmit={handleSubmit}>
                <p className="pstatus">{status}</p>
                <p><input type="text" placeholder="Enter the name" value={name} onChange={e => setName(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter a username" value={username} onChange={e => setUsername(e.target.value)} /></p>
                <p><input type="password" placeholder="Enter a password" value={password} onChange={e => setPassword(e.target.value)} /></p>
                <p><input type="password" placeholder="confirm the password" value={confPassword} onChange={e => setConfPassword(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter the date of birth" value={dob} onChange={e => setDob(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter fathers name" value={father} onChange={e => setFather(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter blood group" value={blood} onChange={e => setBlood(e.target.value)} /></p>
                <p><input type="text" placeholder="Enter the address" value={address} onChange={e => setAddress(e.target.value)} /></p>
                <p>
                    <span>Select the designation:</span>
                    <select onChange={e => setDesignation(e.target.value)}>
                        {jobData.map(i => <option key={i.id} value={i.id}>{i.designation}</option>)}
                    </select>
                </p>
                <p><input type="text" placeholder="Enter the reporting manager" value={rm} onChange={e => setRm(e.target.value)} /></p>
                <button onClick={handleSubmit}>Create new user</button>
            </form>
        </Panel>
    );
}

export default AddUser;