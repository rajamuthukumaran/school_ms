import React, { useState, useEffect } from 'react'
import Panel from './Panel';
import UpdateDetails from './UpdateDetails'
import Axios from 'axios'
import Cookie from 'js-cookie'

const UpdateUser = ({ setPanel, jobData, config, setToast }) => {
    const [search, setSearch] = useState('')
    const [users, loadUsers] = useState([])
    const [dispUsers, setDispUsers] = useState([])
    const API = `${process.env.REACT_APP_URI}:${process.env.REACT_APP_PORT}/api`;

    useEffect(() => {
        Axios.get(API, config)
            .then(res => {
                loadUsers(res.data)
                setDispUsers(res.data)
            })
        if (!users) logout();
    }, [])

    const logout = () => {
        Cookie.remove('userData');
        Cookie.remove('jobData');
        Cookie.remove('Config');
        Cookie.remove('status');
        window.location.reload();
    }

    const handleSearchBar = e => {
        const { value } = e.target;
        setSearch(value);
        const len = value.length;
        let newDisp = users.filter(i => {
            return i.name.slice(0, len).toLowerCase() === value.toLowerCase() ||
                i.username.slice(0, len).toLowerCase() === value.toLowerCase()
        });
        setDispUsers(newDisp);
    }

    const displayUsers = (data) => {
        const { name, username } = data
        return (
            <li key={username} onClick={() => handleUpdate(data)}>{`${name}`}<span>{`(${username})`}</span></li>
        )
    }

    const handleUpdate = user => {
        setPanel(() => <UpdateDetails setPanel={setPanel} setToast={setToast} jobData={jobData} updUser={user} config={config} />)
    }

    return (
        <Panel name="update_user" setPanel={setPanel}>
            <div className="search">
                <form>
                    <input type="text" placeholder="Enter the Name" value={search} onChange={handleSearchBar} />
                    <button>Search</button>
                </form>
            </div>
            <div className="list">
                <form>
                    <ul>
                        {dispUsers.map(i => displayUsers(i))}
                    </ul>
                </form>
            </div>
        </Panel>
    );
}

export default UpdateUser;