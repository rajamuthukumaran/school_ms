import React, { Component } from 'react';
import Login from './Login';
import Student from './SchoolMS';
import './sass/style.scss'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      userData: {},
      config: {},
      jobData: [],
      loggedIN: false
    }
    this.handleUpdate = this.handleUpdate.bind(this)
  }

   handleUpdate(userData, config, jobData){
      this.setState({
        userData,
        config,
        jobData,
        loggedIN: true
      });
   }

  render() { 
    const { userData, jobData, config, loggedIN } = this.state;

    return ( 
      <>
        {(loggedIN)?
          <Student userData={userData} jobData={jobData} config={config}/>:
          <Login onUpdate={this.handleUpdate}/>
        }
      </>
     );
  }
}
 
export default App;