import React, { useState } from 'react';
import Header from './components/Header';   
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Maintenance from './pages/Maintenance';
import Staff from './pages/Staffs';
import Student from './pages/Students';

const SchoolMS = (props) => {
    const { userData, jobData, config } = props
    const { tier } = userData
    return ( 
        <div className="student">
            <Router>
                <Header tier={userData.tier}/>
                <Switch>
                    <Route path="/" exact component={() => <Home userData={userData}/>}/>
                    { tier === 1 && <Route path="/maintenance" component={() => <Maintenance userData={userData} jobData={jobData} config={config}/>}/>}
                    { tier === 1 && <Route path="/staffs" component={() => <Staff jobData={jobData} config={config}/>}/>}
                    { tier !== 3 && <Route path="/students" component={() => <Student config={config}/>}/> }
                </Switch>
            </Router>
        </div>
     );
}
 
export default SchoolMS;