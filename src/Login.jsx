import React, { useState, useEffect, useRef } from 'react';
import Cookies from 'js-cookie';
import './sass/login.scss';
import { login, getJobs, getUserData, getJobData } from './communicator';

const Login = (props) => {
    const [ uname, setUname ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ status, setStatus ] = useState('');

    const inputFocus = useRef(null);

    useEffect(() => {
        inputFocus.current.focus()
        const check = Cookies.get("status");
        if(check){
            const { userData, config, jobData } = Cookies.getJSON()
            props.onUpdate(userData, config, jobData);
        }
    }, []);

    const handleSubmit = async e => {
        e.preventDefault()
        const verify = await login(uname, password);
        if(!verify.status){
            setStatus('Incorrect password or username');
            return
        }

        console.log(verify)

        const config = {
            headers: {
                authtoken: verify.token.data
            }
        }

        const data = await getUserData(uname, config);
        console.log(data )
        const { username, name, dob, father, blood, address, designation, rm } = data.userData.data;

        let sup = 'na';
        if(rm !== 'na'){
            const supData = await getUserData(rm, config)
            sup = supData.userData.data.name;
        }

        const userJobData = await getJobData(designation, config);
        const job = userJobData.jobData.data.designation;
        const tier = userJobData.jobData.data.tier;

        const preJobData = await getJobs(config);
        const jobData = preJobData.jobs.data;
        console.log(jobData)

        const userData = {
            username,
            name,
            dob, 
            father,
            blood,
            address,
            designation,
            job,
            tier,
            sup
        };
        Cookies.set("status", true, {expires: 1});
        Cookies.set("userData", userData, {expires: 1});
        Cookies.set("jobData", jobData, {expires: 1});
        Cookies.set("config", config, {expires: 1});
        props.onUpdate(userData, config, jobData);
    }

    return ( 
        <main className="login">
                <div className="badge"></div>
                <div className="credentials">
                    <form onSubmit={handleSubmit}>
                        <input ref={inputFocus} type="text" placeholder="Enter your username" value={uname} onChange={e => setUname(e.target.value)}/>
                        <input type="password" placeholder="Enter the password" value={password} onChange={e => setPassword(e.target.value)}/>
                        <button className="button" onClick={handleSubmit}>Login</button>
                        <p className="login_status">{status}</p>
                    </form>
                </div>
            </main>
     );
}
 
export default Login;